function getCurrentTime() {
    const now = new Date();

    let currentTime = now.getHours() + ':' + now.getMinutes();

    return currentTime;
}

function getCurrentDate() {
    let now = new Date();
    let dd = now.getDate();
    let mm = now.getMonth()+1;
    let yyyy = now.getFullYear();

    if(dd<10) {
        dd = '0'+dd
    } 

    if(mm<10) {
        mm = '0'+mm
    } 

    const currentDate = yyyy + '-' + mm + '-' + dd;

    return currentDate;
}

function getType(inputType) {

    if(inputType == 'Bus') {
        var useBusValue = 1;
    } else {
        var useBusValue = 0; 
    }

    if(inputType == 'Boat') {
        var useBoatValue = 1;
    } else {
        var useBoatValue = 0;
    }

    if(inputType == 'Tram') {
        var useTramValue = 1;
    } else {
        var useTramValue = 0;
    }

    if(inputType == 'Vas'){
        var useVasValue = 1;
    } else {
        var useVasValue = 0;
    }

    let typeList = {
        useBus: useBusValue,
        useBoat: useBoatValue,
        useTram: useTramValue,
        useVas: useVasValue
    };

    let type = Object.keys(typeList).map(function(k) {
        return encodeURIComponent(k) + '=' + encodeURIComponent(typeList[k])
    }).join('&');

    return decodeURI(type);
}

function getLocation(inputLocation){
    let location = inputLocation;

    return location;
}

function getCategory(inputCategory) {
    if(inputCategory == 'departure'){
        var category = 'departureBoard';
    } else {
        var category = 'journeyDetail';
    }

    return category;
}

export default function (inputLocation, inputType, inputCategory) {

    let category = getCategory(inputCategory);

    let currentTime = getCurrentTime();
    let currentDate = getCurrentDate();

    let type = getType(inputType);
    let location = getLocation(inputLocation);

    let uriList = {
        id: location,
        date: currentDate,
        time: currentTime,
        type
    };

    let UriCollect = Object.keys(uriList).map(function(k) {
        return encodeURIComponent(k) + '=' + encodeURIComponent(uriList[k])
    }).join('&');

    const UriRaw = category + '?' + UriCollect + '&format=json';

    return UriRaw;
}