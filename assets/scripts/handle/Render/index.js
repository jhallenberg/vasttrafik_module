import { fetchTimetable } from '../Fetch';

export async function renderCurrentTimetable(containers) {

    let index = 0;

    for( const currentContainer of containers ) {

        const listTimetable = await fetchTimetable(currentContainer.getAttribute('location'), currentContainer.getAttribute('type'), currentContainer.getAttribute('category')).then(res => { return res });

        currentContainer.setAttribute('data-index', index);

        if ( listTimetable.length ) {
            let htmlOutput = `
                <table>
                ${listTimetable.map(res => `
                <tr>
                    <td style="background-color: ${res.color}; ${res.num == 2 ? 'color: #000;' : ''}">
                        ${res.num}
                    </td>
                    <td>
                        ${res.destination.indexOf("via") > 0 ? res.destination.split("via")[0] : res.destination}
                        ${res.destination.indexOf("via") > 0 ? '<div class="vt_timetable_underline">Via ' + res.destination.split("via")[1] + '</div>' : ''}
                        ${res.destination == "Högsbotorp" || res.destination == "Angered" ? '<div class="vt_timetable_underline">Via Centralstationen</div>' : ''}
                        ${res.destination == "Heden" ? '<div class="vt_timetable_underline">Via Korsvägen</div>' : ''}
                        ${res.destination == "Förlunda Torg" ? '<div class="vt_timetable_underline">Via Bifrost</div>' : ''}
                    </td>
                    ${res.time.slice(0, 2).map(time => `
                            <td>
                                ${time ? time >= 60 ? '60+' : time < 3 ? time < 0 ? '60+' : 'Nu' : time : '-'}
                            </td>
                    `).join('')}
                    ${res.time.length < 2 ? `
                            <td>
                                -
                            </td>
                    ` : ''}
                    <td class="type ${res.type.toLowerCase()}">
                    </td>
                </tr>
                `).join('')}
                </table>`;

            document.querySelector( '.vt_timetable[data-index="'+index+'"]' ).innerHTML = htmlOutput;

        } else {
            let htmlOutput = `
                <div class="no-results">
                    Inga tillgängliga resor inom den närmaste timmen från den här hållplatsen.
                </div>
            `;

            document.querySelector( '.vt_timetable[data-index="'+index+'"]' ).innerHTML = htmlOutput;
        }

        index++;
    };
}