import { callTimetable } from '../Call';
import Uri from '../Uri';

var moment = require('moment');

const groupedTimetable = [];
let listEmpty = false;

function minutesFromNow(timetableTime) {

    var timeDifference = moment(timetableTime, "HH:mm").diff(moment(), 'minutes');

    return timeDifference;
}

function checkDuplication(api) {
    var i = null;

    for (i = 0; groupedTimetable.length > i; i += 1) {
        if (groupedTimetable[i].destination === api.direction && groupedTimetable[i].num === api.sname) {
            return i;
        }
    }
     
    return false;
};

function mergeTimetable(fullTimetable) {
    for ( var i in fullTimetable ) {
        const duplicationAnswer = checkDuplication(fullTimetable[i]);

        if(duplicationAnswer !== false){
            let time = minutesFromNow(fullTimetable[i].time);

            groupedTimetable[duplicationAnswer].time.push(time);
        } else {
            if ( minutesFromNow(fullTimetable[i].time) > 0 ) {
                groupedTimetable.push({
                    'destination': fullTimetable[i].direction,
                    'color': fullTimetable[i].fgColor,
                    'num': fullTimetable[i].sname,
                    'type': fullTimetable[i].type,
                    'time': [minutesFromNow(fullTimetable[i].time)]
                });
            }
        }
    }
}

function compare(a,b) {
    if (a.num < b.num)
      return -1;
    if (a.num > b.num)
      return 1;
    return 0;
  }

export async function fetchTimetable(location, type, category) {

    const fullTimetable = await callTimetable(Uri(location, type, category));

    if ( listEmpty != true ){
        groupedTimetable.length = 0;

        listEmpty = true;
    }

    await mergeTimetable(fullTimetable);
      
    groupedTimetable.sort(compare);
          
    listEmpty = false;

    return Promise.resolve(groupedTimetable);
}