const superagent = require('superagent');

function setAccessToken(accessToken) {
    if(sessionStorage.getItem('token') == null) {
        sessionStorage.setItem('token', accessToken);
    } else {        
        sessionStorage.removeItem('token');
        sessionStorage.setItem('token', accessToken);

        return false;
    }
}

export function getCurrentToken() {
    if(sessionStorage.getItem('token') !== null) {
        let currentToken = sessionStorage.getItem('token');

        return currentToken;
    } else {
        return false;
    }
}

export function callAuth(credentials, deviceId) {
    let request = superagent
                    .post('https://api.vasttrafik.se/token')
                    .set({Authorization: 'Basic ' + credentials})
                    .send('grant_type=client_credentials&scope=device_' + deviceId);

    return request.then(res => {
        if (!res.ok) {
            return Promise.reject(res.error);
        } else {
            let accessToken = res.body.access_token;

            setAccessToken(accessToken);
            
            return Promise.resolve(accessToken);
        }
    });
}

export function callTimetable(uri) {
    let accessToken = getCurrentToken();

    let request = superagent
                    .get('https://api.vasttrafik.se/bin/rest.exe/v2/' + uri)
                    .set({Authorization: 'Bearer ' + accessToken})

    return request.then(res => {
        if (!res.ok) {
            return Promise.reject(res.error);
        } else {
            let result = res.body.DepartureBoard.Departure;
            
            return Promise.resolve(result);
        }
    });
}