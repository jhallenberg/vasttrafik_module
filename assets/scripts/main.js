import { callAuth } from './handle/Call';
import { renderCurrentTimetable } from './handle/Render';

import '../style/main.scss';

const Base64 = require('Base64');
const interval = require('interval-promise');

let key = 'whlxUTOg_znQUFZWJD7IeWFw1hAa';
let secret = '1FIE_AGX9iWCFws48AHA4w_ijFca';

let credentials = Base64.btoa(key + ':' + secret);
let deviceId = new Date().getTime();

async function runApplication() {
    if( document.querySelectorAll(".vt_timetable").length ) {
        const token = await callAuth(credentials, deviceId).then(res => { return res });

        const containers = document.querySelectorAll(".vt_timetable");

        await renderCurrentTimetable(containers);

        const updateEveryMinute = interval(async () => {
                                    await renderCurrentTimetable(containers);
                                }, 60*1000, {iterations: 5});
    }
}

document.addEventListener("DOMContentLoaded", runApplication);