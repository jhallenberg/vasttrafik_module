<?php

/*
Plugin Name: Västtrafiken Wordpress Module
Plugin URI: http://codex.wordpress.org/Adding_Administration_Menus
Description: Plugin to, by shortcode, render dynamic timetable on module in Wordpress-theme.
Author: Johan Hallenberg for Sultan & Sultan.
Author URI: http://sultan.se
Version: 1.0.0
*/

defined( 'ABSPATH' ) or die( 'No access' );

// Add menu to adminpanel
add_action( 'admin_menu', 'vt_module' );

function vt_module() {
	add_menu_page( 'Västtrafiken Wordpress Module', 'Tidtabell', 'manage_options', 'vt_admin', 'vt_options' );
}

function vt_options() {
    include_once( 'admin/vt_admin_display.php' );
}

// Add JS-script and styling from plugin
add_action('wp_enqueue_scripts','vt_load_scripts');

function vt_load_scripts() {
    wp_enqueue_script( 'vt_scripts', plugins_url( '/dist/script.js', __FILE__ ));

    wp_enqueue_style( 'vt_style', plugins_url( '/dist/style.css', __FILE__ ));
}

// Add shortcode to use when rendering timetable
add_shortcode('tidtabell', 'timetable_add_shortcode');

function timetable_add_shortcode($atts) {
	// Attributes
	$atts = shortcode_atts(
		array(
			'location' => 'Centralstationen',
			'type' => 'Tram',
			'category' => 'Departure',
		),
		$atts,
		'tidtabell'
    );

    $htmlOutput = '<div class="vt_timetable" location="' . $atts['location']  . '" type="' . $atts['type'] . '" category="' . $atts['category'] . '"></div>';

    return $htmlOutput;
}

add_shortcode('vasttrafiken', 'vt_add_shortcode');

function vt_add_shortcode($content) {
	// Attributes

    $timetables = strip_shortcodes( get_the_content() );

    $htmlOutput = '<div id="vt_timetable_wrapper">
                    ' . $timetables . '
                   </div>';

    return $htmlOutput;
}

?>
