# README #

## Wordpress Västrafiken Module?

A Wordpress API-module for Västtrafiken timetable. The plugin will render timetable by each mark-up you add. 

## Install plugin

Install plugin by normal use with Wordpress (aka. add vasttrafik_module inside `plugins`-folder).

### Add mark-up by HTML 

Wrap the mark-ups with an overall wrapper.

```
<div id="vt_timetable_wrapper"> // Add the timetable-tags here</div>
```

Then add your timetables. You can add as many as you want to.

```
 <div class="vt_timetable" location="Lana" type="Tram" category="departure"></div>
```

| Options       | Purpose                    | Value          |
| ------------- |:--------------------------:| --------------:|
| Location      | Define stop                | tex. Lana      |
| Type          | What type of transportion  | tex. Tram      |
| Category      | Is it departure or arrival | tex. departure |

### Example

```
 <div id="vt_timetable_wrapper"> 
    <div class="vt_timetable" location="Lana" type="Tram" category="departure"></div>
    <div class="vt_timetable" location="Centralen" type="Bus" category="departure"></div>
 </div>
```

### Add mark-up by shortcode 

Wrap the mark-ups with an overall wrapper.

```
[vasttrafiken] // Add the timetable-tags here [/vasttrafiken]
```

Then add your timetables. You can add as many as you want to.

```
 [vt_timetable location="Lana" type="Tram" category="departure"]
```

| Options       | Purpose                    | Value          |
| ------------- |:--------------------------:| --------------:|
| Location      | Define stop                | tex. Lana      |
| Type          | What type of transportion  | tex. Tram      |
| Category      | Is it departure or arrival | tex. departure |

### Example

```
 [vasttrafiken]
    [vt_timetable location="Lana" type="TRAM" category="departure"]
    [vt_timetable location="Centralen" type="BUS" category="departure"]
 [/vasttrafiken]
```

## Edit plugin

Install by `npm install` and then run webpack by `npm run dev`. Send to production with `npm run build`.