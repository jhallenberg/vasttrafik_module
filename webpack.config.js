const ExtractTextPlugin = require("extract-text-webpack-plugin");
const path = require('path');
var glob = require("glob");

const extractSass = new ExtractTextPlugin({
    filename: "style.css",
    disable: process.env.NODE_ENV === "development"
});

module.exports = {
    entry: {
        'script': glob.sync("./assets/scripts/*.js")
    },
      output: {
        path: path.resolve(__dirname, 'dist'),
        filename: '[name].js'
    },
  module: {
      rules: [{
              test: /\.js?/,
              include: path.resolve(__dirname, 'js'),
              exclude: /node-modules/,
              use: [{
                  loader: "babel-loader",
                  query: {
                    presets: ["es2015", "stage-2"]
                  }
              }]
          },{
              test: /\.scss$/,
              use: extractSass.extract({
                use: [{
                    loader: "css-loader"
                },{
                    loader: "sass-loader"
                }],
                fallback: "style-loader"
             })
          },
          {
              test: /\.(woff(2)?|ttf|eot)(\?v=\d+\.\d+\.\d+)?$/,
              use: [{
                  loader: 'file-loader',
                  options: {
                      name: '[name].[ext]',
                      outputPath: 'fonts/'
                  }
              }]
          },
          {
            test: /\.(jpe?g|png|gif|svg)$/i, 
            loader: 'file-loader',
            options: {
                name: '[name].[ext]',
                outputPath: 'images/'
            }
        }]
    },
      plugins: [
        extractSass
    ]
};